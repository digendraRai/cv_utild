#include <cstdlib>
#include <png.h>
#include <cstring>

// Performes a floor by casting to an int. Should only be used with values > 0.
#define FLOOR(x) ((int)(x))

// Linear interpolates x between start and end.
#define LINEAR_OP(start, end, x) (start + (end - start) * x)

using namespace std;

enum ResizeMethod 
{
    NEARSET_NEIGHBOR,
    BILINEAR,
};

struct Image
{
    unsigned char* data;
    int width;
    int height;
    int channels;
    unsigned long size;
    unsigned long lineSize;
};