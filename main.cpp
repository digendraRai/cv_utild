#include "png_func.cpp"
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[]) {
    
    char* infile = argv[1];
    char* outfile = argv[2];
    
    Png_Func png_func;
    Image* image, *im1;
    
    image = png_func.read(infile);

    im1 = png_func.resize(image, 512,128,BILINEAR);
    
    png_func.write(im1, outfile);
 
	return 0;
}