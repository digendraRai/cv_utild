#include "image_info.h"

using namespace std;

class Png_Func
{
    public: 
        Image* read(const char* path);
        void write(Image* image, const char* path);
        Image* fill_image_data(unsigned int width, unsigned int height, unsigned int channels, unsigned char* data);     
        
        Image* resize(Image* image_src, unsigned int width, unsigned int height, ResizeMethod method);
        void resize_nearest(const Image* image, unsigned char* out, unsigned int targetWidth, unsigned int targetHeight);
        void resize_bilinear(const Image* image, unsigned char* out, unsigned int targetWidth, unsigned int targetHeight);
        
        float bilinear_op(float c00, float c10, float c01, float c11, float x, float y);
        
        Image* im;
};