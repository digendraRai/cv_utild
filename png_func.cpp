#include "png_func.h"

using namespace std;

float Png_Func::bilinear_op(float c00, float c10, float c01, float c11, float x, float y) 
{
    return LINEAR_OP(LINEAR_OP(c00, c10, x), LINEAR_OP(c01, c11, x), y);
}

void Png_Func::resize_nearest(const Image* image, unsigned char* out, unsigned int targetWidth, unsigned int targetHeight) 
{
    // Pre-calc some constants
    const double xRatio = image->width / (double)targetWidth;
    const double yRatio = image->height / (double)targetHeight;
    const unsigned int originalLineSize = image->width * image->channels;
    const unsigned int newRowSize = targetWidth * image->channels;

    const auto channels = image->channels;

    // Go through each image line
    int newStart, oldStart;
    int scaledOriginalLineSize;
    for (unsigned int y = 0; y < targetHeight; y++) 
    {
        scaledOriginalLineSize = FLOOR(y * yRatio) * originalLineSize;

        for (unsigned int x = 0; x < targetWidth; x++) 
        {
            // calc start index of old and new pixel data
            newStart = y * newRowSize + x * channels;
            oldStart = scaledOriginalLineSize + FLOOR(x * xRatio) * channels;

            // copy values from the old pixel array to the new one
            memcpy(out + newStart, image->data + oldStart, channels);
        }
    }
}

void Png_Func::resize_bilinear(const Image* image, unsigned char* out, unsigned int targetWidth, unsigned int targetHeight) 
{
    const auto data = image->data;
    const auto channels = image->channels;

    const unsigned int newLineSize = targetWidth * channels;
    const unsigned int oldLineSize = image->width * channels;

    for (unsigned int y = 0; y < targetHeight; y++) 
    {
        unsigned int oldY = y / (float)(targetHeight) * (image->height - 1);
        float newYScale = (float)y / targetHeight;
        unsigned int currentLineOffset = y * newLineSize;

        for (unsigned int x = 0; x < targetWidth; x++) 
        {
            unsigned int oldX = x / (float)(targetWidth) * (image->width - 1);

            unsigned int c00 = (oldX * channels) + (oldY * oldLineSize);
            unsigned int c10 = c00 + channels;
            unsigned int c01 = c00 + oldLineSize;
            unsigned int c11 = c01 + channels;

            unsigned int newStart = currentLineOffset + x * channels;
            float newXScale = (float)x / targetWidth;
            for (int i = 0; i < channels; i++) 
            {
                out[newStart + i] = bilinear_op(data[c00 + i],
                                       data[c10 + i],
                                       data[c01 + i],
                                       data[c11 + i],
                                       newXScale,
                                       newYScale);
            }
        }
    }
}

Image* Png_Func::resize(Image* image_src, unsigned int width, unsigned int height, ResizeMethod method) 
{
    // allocate size to new data
    unsigned char* imageBuffer = new unsigned char[sizeof(unsigned char) * width * height * image_src->channels];

    // perform operation
    if (method == ResizeMethod::NEARSET_NEIGHBOR) 
    {
        resize_nearest(image_src, imageBuffer, width, height);
    } 
    else if (method == ResizeMethod::BILINEAR) 
    {
        resize_bilinear(image_src, imageBuffer, width, height);
    }

    // update image
    free(image_src->data);
    image_src->data = imageBuffer;
    image_src->width = width;
    image_src->height = height;
    image_src->lineSize = width * image_src->channels;
    image_src->size = height * image_src->lineSize;

    return image_src;
}


Image* Png_Func::fill_image_data(unsigned int width, unsigned int height, unsigned int channels, unsigned char* data)
{   
    Image* img = new Image();
    
    img->data = data;
    img->width = width;
    img->height = height;
    img->channels = channels;
    img->size = height * width * channels;
    img->lineSize = channels * width;
    
    return img;
}

Image* Png_Func::read(const char* path) 
{        
    // open file
    FILE* file = fopen(path, "rb");
    if (!file)
        printf("Failed to read file \n");

    // verify header
    unsigned char header[8];
    fread(header, 1, 8, file);
    if (png_sig_cmp(header, 0, 8))
        printf("Invalid png file \n");

    // setup png structures
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!png_ptr)
        printf("png_create_read_struct failed \n");

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
        printf("png_create_info_struct failed \n");

    png_init_io(png_ptr, file);
    png_set_sig_bytes(png_ptr, 8);
    png_read_info(png_ptr, info_ptr);

    unsigned char bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    if (bit_depth != 8)
        printf("Error: bit_depth != 8");

    int width = png_get_image_width(png_ptr, info_ptr);
    int height = png_get_image_height(png_ptr, info_ptr);
    unsigned char channels = png_get_channels(png_ptr, info_ptr);
    png_read_update_info(png_ptr, info_ptr);

    // read file
    if (setjmp(png_jmpbuf(png_ptr)))
        printf("Error during read_image \n");

    unsigned char *row_pointers[height];
    unsigned int rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    unsigned char *image_data = new unsigned char[rowbytes * height];

    for (int i = 0; i < height; i++) 
    {
        row_pointers[i] = image_data + i * rowbytes;
    }

    png_read_image(png_ptr, row_pointers);
    png_read_end(png_ptr, NULL);
    fclose(file);

    im = fill_image_data(width, height, channels, image_data);
        
    return im;
}

void Png_Func::write(Image* image, const char* path)    
{
    // create file
    FILE* file = fopen(path, "wb");
    if (!file)
        printf("Failed to open file for writing");

    // initialize png structures
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr)
        printf("png_create_write_struct failed");

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
        printf("png_create_info_struct failed");

    png_init_io(png_ptr, file);

    int color_type = (image->channels == 3) ? PNG_COLOR_TYPE_RGB : PNG_COLOR_TYPE_RGB_ALPHA;
    png_set_IHDR(png_ptr,
                info_ptr,
                image->width,
                image->height,
                8,
                color_type,
                PNG_INTERLACE_NONE,
                PNG_COMPRESSION_TYPE_DEFAULT,
                PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);

    // create row pointers
    unsigned char *row_pointers[image->height];
    unsigned int rowbytes = image->channels * image->width;
    for (int i = 0; i < image->height; i++) 
    {
         row_pointers[i] = image->data + i * rowbytes;
    }

    // write image bytes
    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, NULL);

    fclose(file);
}